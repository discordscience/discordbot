/*

    DiscordBot
    Copyright (C) 2018  Discord Science and Technology Community

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package discordscience.discordbot;

import java.io.*;
import java.time.*;
import java.util.*;
import java.util.concurrent.*;
import java.lang.reflect.Constructor;
import net.md_5.bungee.config.*;
import net.cubekrowd.eventstorageapi.api.*;
import net.cubekrowd.eventstorageapi.api.event.*;
import net.cubekrowd.eventstorageapi.common.*;
import net.cubekrowd.eventstorageapi.common.storage.*;
import net.dv8tion.jda.core.*;
import net.dv8tion.jda.core.entities.*;
import net.dv8tion.jda.core.exceptions.*;
import net.dv8tion.jda.core.utils.*;
import net.dv8tion.jda.core.requests.*;
import org.apache.logging.log4j.LogManager;
import com.github.fcannizzaro.material.Colors;
import lombok.*;

@lombok.extern.log4j.Log4j2
public class Bot {

    @Getter private final OffsetDateTime startTime = OffsetDateTime.now();
    @Getter private final ScheduledExecutorService scheduler;
    @Getter private final List<Plugin> plugins;
    @Getter private final Configuration config;
    @Getter private final JDA jda;

    public Bot() {

        // Debug
        log.info("Initialising bot...");
        RestAction.setPassContext(true);
        RestAction.DEFAULT_FAILURE = Throwable::printStackTrace;

        // Load config
        log.info("Reading config...");
        try {
            config = ConfigurationProvider.getProvider(YamlConfiguration.class).load(new File("config.yml"));
        } catch(IOException e) {
            throw new RuntimeException("Unable to read config file");
        }

        // Load Scheduler
        log.info("Setting up scheduler...");
        scheduler = Executors.newScheduledThreadPool(getConfig().getInt("scheduler-pool"));

        // Init ESAPI
        log.info("Connecting to database...");
        EventStorageAPI.registerInit(new StorageMongoDB(new EventStoragePlugin() {
            public List<String> getAuthors() {
                return null;
            }
            public String getVersion() {
                return null;
            }
            public void callEvent(ESAPIEvent event, Object... data) {
            }
        }, "127.0.0.1", 27017, "eventstorage", "", ""));
        EventStorageAPI.getStorage().open();

        // Connect to Discord
        log.info("Connecting to discord...");
        JDABuilder jdab = null;
        try {
            jdab = new JDABuilder(AccountType.BOT)
            .setAudioEnabled(false)
            .setToken(getConfig().getString("discord-token"))
            .addEventListener(new BotEventListener(this));
        } catch (Exception e) {
            log.fatal("Error while initializing bot", e);
            System.exit(-1);
        }

        // Load plugins
        log.info("Loading plugins...");
        plugins = loadPlugins(jdab);
        log.info("Activated {} plugins.", plugins.size());

        JDA jda2 = null; //ugh
        try {
            // Build/Connect
            jda2 = jdab.buildBlocking();
        } catch (Exception e) {
            log.fatal("Error while initializing bot", e);
            System.exit(-1);
        } finally {
            jda = jda2;
        }

        // Setup shutdown hook
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                log.info("Running shutdown hook.");
                jda.shutdown();
                log.info("Closing storage.");
                EventStorageAPI.getStorage().close();
                log.info("Shutdown hook done. Bye.");
                jda.shutdownNow();
                LogManager.shutdown();
            }
        });
    }

    public List<Plugin> loadPlugins(JDABuilder jdab) {
        List<Plugin> plugins = new ArrayList<>();
        for (String label : getConfig().getStringList("command-list")) {
            try {
                Class<?> clazz = Class.forName("discordscience.discordbot.plugin." + label);
                Constructor<?> ctor = clazz.getConstructor(Bot.class);
                Plugin plugin = (Plugin) ctor.newInstance(new Object[] { this });
                plugins.add(plugin);
                jdab.addEventListener(plugin);
            } catch(Exception e) {
                log.error("Failed to load plugin \"" + label + "\".", e);
                System.exit(1);
            }
            log.debug("PLUGIN \tLoaded plugin: \t{}", label);
        }
        return plugins;
    }

    public void executeCommand(Message message) {
        // Ignore messages from bots
        if(message.getAuthor().isBot()) {
            return;
        }

        // Split up into arguments
        String msg = message.getContentRaw().replace("\n", "").replace("\r", "").replace("`", "`\u200B");
        String prefix = getConfig().getString("command-prefix");
        if(!msg.startsWith(prefix)) {
            return;
        }

        // Only allow commands on guilds
        if(message.getGuild() == null) {
            message.getChannel().sendMessage(new EmbedBuilder().setColor(Colors.red_500.asColor()).setDescription("\u2757 **Invalid Usage**\nCommands can only be executed in a guild.").build()).queue();
            return;
        }

        String[] args = msg.split(" ");
        String label = args[0].substring(1).toLowerCase();
        args = Arrays.copyOfRange(args, 1, args.length);
        for(Plugin plugin : plugins) {
            if(plugin.executeCommand(message, label, args)) {
                log.info("{}#{} <{}> issued command: {}", message.getAuthor().getName(), message.getAuthor().getDiscriminator(), message.getAuthor().getId(), msg);
                return;
            }
        }
        log.warn("Unknown command '{}'", label);
    }

    public void deleteMessage(Message message, int time, TimeUnit unit) {
        getScheduler().schedule(() -> {try {
            message.delete().complete();
        } catch(ErrorResponseException ignored) {}
                                      }, time, unit);
    }

}
