/*

    DiscordBot
    Copyright (C) 2018  Discord Science and Technology Community

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package discordscience.discordbot;

import java.util.*;
import net.dv8tion.jda.core.utils.*;
import lombok.*;

@lombok.extern.log4j.Log4j2
public class Main {

    public static void main(String[] args) {

        log.trace("TRACE");
        log.debug("DEBUG");
        log.error("ERROR");
        log.fatal("FATAL");
        log.info("INFO");
        log.warn("WARN");

        new Bot();

    }

}
